import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  ScaledSheet,
  scale,
  verticalScale,
  moderateScale,
} from 'react-native-size-matters';
import CalendarPicker from 'react-native-calendar-picker';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: null,
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date) {
    this.setState({
      selectedStartDate: date,
    });
  }
  render() {
    const minDate = new Date();
    const {selectedStartDate} = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';
    return (
      <View style={styles.container}>
        <CalendarPicker
          minDate={minDate}
          // selectedDayColor="#FE3650"
          selectedDayTextColor="#FFFFFF"
          onDateChange={this.onDateChange}
          todayBackgroundColor="#FE3650"
          previousTitle=" "
          nextTitle=" "
          
          
        />

        {/* <View>
          <Text>SELECTED DATE:{startDate}</Text>
        </View> */}
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    marginTop: verticalScale(10),
  },
});
