import React, {Component} from 'react';
import {Text, Dimensions, TouchableOpacity} from 'react-native';
import {
  ScaledSheet,
  scale,
  verticalScale,
  moderateScale,
} from 'react-native-size-matters';
const {width: WIDTH} = Dimensions.get('window');

export default CustomButton = ({buttonText, color, textColor, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.button,
        {
          backgroundColor: color,
          borderWidth: color === 'transparent' ? 2 : 0,
          borderColor: color === 'transparent' ? 'white' : null,
        },
      ]}>
      <Text style={[styles.textButton, {color: textColor}]}>{buttonText}</Text>
    </TouchableOpacity>
  );
};
const styles = ScaledSheet.create({
  button: {
    width: WIDTH - scale(30),
    height: verticalScale(50),
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: scale(15),
    marginRight: scale(15),
    borderRadius: moderateScale(4),
  },
  textButton: {
    color: 'black',
    fontSize: moderateScale(18),
    fontWeight: 'bold',
  },
});
