import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  ScaledSheet,
  scale,
  verticalScale,
  moderateScale,
} from 'react-native-size-matters';
import CustomButton from './Component/CustomButton';
import Calender from './Component/Calender';

export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 0.5}}>
          <Calender/>
        </View>
        <View style={{flex: 0.5}}>
          <CustomButton
            buttonText={'Confirm'}
            color="#FE3650"
            textColor="white"
          />
        </View>
      </View>
    );
  }
}
